<?php

namespace Coosos\VersioningWorkflowBundle\DependencyInjection;

use Coosos\VersioningWorkflowBundle\Configuration\WorkflowConfiguration as VWConfiguration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package Coosos\VersioningWorkflowBundle\DependencyInjection
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class Configuration implements ConfigurationInterface
{
    const ROOT = "coosos_versioning_workflow";

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(self::ROOT);
        $rootNode
            ->children()
            ->arrayNode("list")->arrayPrototype()->children()
            ->arrayNode(VWConfiguration::SERIALIZABLE_GROUPS)->scalarPrototype()->end()->end()
            ->arrayNode(VWConfiguration::STEPS)->isRequired()->scalarPrototype()->end()->end()
        ;

        return $treeBuilder;
    }
}
