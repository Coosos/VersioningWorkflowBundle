<?php

namespace Coosos\VersioningWorkflowBundle\Serializer;

use Coosos\VersioningWorkflowBundle\Serializer\Adapter\SerializerInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

/**
 * Class Serializer
 *
 * @package Coosos\VersioningWorkflowBundle\Serializer
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class Serializer implements SerializerInterface
{
    /**
     * @var \Symfony\Component\Serializer\Serializer serializer
     */
    private $serializer;

    /**
     * @var array groups
     */
    private $groups;

    public function __construct(SymfonySerializerInterface $serializer, array $groups = [])
    {
        $this->serializer = $serializer;
        $this->groups = $groups;
    }

    public function serialize($object, $groups = [])
    {
        $groups = empty($groups = array_merge($this->groups, $groups)) ? null : $groups;

        return $this->serializer->serialize($object, 'json', ['groups' => $groups, 'enable_max_depth' => true]);
    }

    public function deserialize($object, $class)
    {
        return $this->serializer->deserialize($object, $class, 'json');
    }
}
