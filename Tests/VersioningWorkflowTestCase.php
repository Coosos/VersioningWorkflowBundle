<?php

namespace Coosos\VersioningWorkflowBundle\Tests;

use Coosos\VersioningWorkflowBundle\Configuration\WorkflowConfiguration;
use Coosos\VersioningWorkflowBundle\DependencyInjection\Configuration;
use Coosos\VersioningWorkflowBundle\Entity\VersioningWorkflow;
use Coosos\VersioningWorkflowBundle\Process\Process;
use Coosos\VersioningWorkflowBundle\Tests\Model\News;
use Coosos\VersioningWorkflowBundle\Tests\Model\User;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Coosos\VersioningWorkflowBundle\Serializer\Serializer as SerializerAdapter;
use Symfony\Component\Yaml\Yaml;

/**
 * Class VersioningWorkflowTestCase
 *
 * @package Coosos\VersioningWorkflowBundle\Tests
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class VersioningWorkflowTestCase extends TestCase
{
    const WORKFLOW_NAME = 'news';
    const ENTITY_PATH = News::class;

    /**
     * @var SerializerAdapter serializer
     */
    protected $serializer;

    protected function setUp()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $this->serializer = new SerializerAdapter($serializer);
    }

    /**
     * Generate news
     *
     * @param int[null $id
     * @param User     $user
     * @return News
     */
    protected function generateNews($id = null, User $user = null)
    {
        return (new News())
            ->setId($id)
            ->setUser($user)
            ->setTitle('Hello world')
            ->setContent('I do not know what to say, sorry :(');
    }

    /**
     * Get Process
     *
     * @param $mockEntityManager
     * @return Process
     */
    protected function getProcess($mockEntityManager)
    {
        return new Process(
            $this->serializer,
            $mockEntityManager,
            $this->getExampleConfiguration()[0],
            self::ENTITY_PATH
        );
    }

    /**
     * @return EntityManager|\PHPUnit\Framework\MockObject\MockObject
     * @throws \ReflectionException
     */
    protected function generateMockEntityManager()
    {
        return $this->createMock(EntityManager::class);
    }

    /**
     * @param News   $news
     * @param int    $instance
     * @param string $actualStatus
     * @param string $workflowName
     * @return VersioningWorkflow
     */
    protected function generateVersioningWorkflow(
        News $news,
        int $instance = 1,
        string $actualStatus = 'editor',
        string $workflowName = self::WORKFLOW_NAME
    ) {
        $serialize = $this->serializer->serialize($news);

        $versioningWorkflow = new VersioningWorkflow();
        $versioningWorkflow
            ->setWorkflowName($workflowName)
            ->setClassPath(self::ENTITY_PATH)
            ->setInstance($instance)
            ->setActualStatus($actualStatus)
            ->setJsonContent($serialize);

        return $versioningWorkflow;
    }

    /**
     * @return array
     */
    protected function getExampleConfiguration()
    {
        $config = [];
        $finder = new Finder();
        $finder->files()->in(__DIR__ . "/fixtures");

        $fixtures = [];
        foreach ($finder as $file) {
            $fixtures[] = $file->getContents();
        }

        foreach ($fixtures as $fixture) {
            $parse = Yaml::parse($fixture);
            foreach ($parse[Configuration::ROOT]["list"] as $key => $value) {
                $config[] = new WorkflowConfiguration($key, $value);
            }
        }

        return $config;
    }
}
