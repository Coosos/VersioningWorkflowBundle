<?php

namespace Coosos\VersioningWorkflowBundle\Tests\Model;

use Coosos\VersioningWorkflowBundle\Process\EntityDeserializer;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class News
 *
 * @package Coosos\VersioningWorkflowBundle\Tests\Model
 * @author  Remy Lescallier <lescallier1@gmail.com>
 */
class News implements EntityDeserializer
{
    private $id;
    private $title;
    private $content;
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return News
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     *
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param News | mixed           $object
     * @param EntityManagerInterface $entityManager
     *
     * @return News
     */
    public function versioningWorkflowDeserialize($object, EntityManagerInterface $entityManager)
    {
        if ($object->getUser() !== null) {
            $user = new User();
            $user->setId(1)->setUsername('Remy');
            $object->setUser($user);
        }

        return $object;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return News
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
