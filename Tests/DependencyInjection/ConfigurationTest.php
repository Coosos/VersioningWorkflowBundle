<?php

namespace Coosos\VersioningWorkflowBundle\tests\DependencyInjection;

use Coosos\VersioningWorkflowBundle\DependencyInjection\Configuration;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigurationTest
 *
 * @package Coosos\TemporaryEntityStorageWorkflowBundle\tests\DependencyInjection
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * @var array fixtures
     */
    private $fixtures;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $finder = new Finder();
        $finder->files()->in(__DIR__ . "/../fixtures");

        $fixtures = [];
        foreach ($finder as $file) {
            $fixtures[] = $file->getContents();
        }

        $this->fixtures = $fixtures;
    }

    /**
     * Test yaml configuration
     */
    public function test()
    {
        foreach ($this->fixtures as $fixture) {
            $parse = Yaml::parse($fixture);
            $processor = new Processor();
            try {
                $processor->processConfiguration(new Configuration(), $parse);
                self::assertEquals(0, 0);
            } catch (InvalidConfigurationException $e) {
                self::assertEquals(0, $e->getMessage());
            }
        }
    }
}
