<?php

namespace Coosos\VersioningWorkflowBundle\Configuration;

/**
 * Class VersioningWorkflowConfiguration
 *
 * @package Coosos\VersioningWorkflowBundle\Configuration
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class WorkflowConfiguration
{
    const STEPS = 'steps';
    const SERIALIZABLE_GROUPS = 'serializable_groups';

    /**
     * @var string workflowName
     */
    private $workflowName;

    /**
     * @var string[] steps
     */
    private $steps;

    /**
     * @var string[] serializableGroups
     */
    private $serializableGroups;

    /**
     * VersioningWorkflowConfiguration constructor.
     * @param string $workflowName
     * @param array  $configurations
     */
    public function __construct(string $workflowName, array $configurations)
    {
        $this->workflowName = $workflowName;
        $this->steps = $configurations[self::STEPS];
        $this->serializableGroups = $configurations[self::SERIALIZABLE_GROUPS];
    }

    /**
     * @return string
     */
    public function getWorkflowName(): string
    {
        return $this->workflowName;
    }

    /**
     * @param string $workflowName
     * @return WorkflowConfiguration
     */
    public function setWorkflowName(string $workflowName): WorkflowConfiguration
    {
        $this->workflowName = $workflowName;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getSteps(): array
    {
        return $this->steps;
    }

    /**
     * @param string[] $steps
     * @return WorkflowConfiguration
     */
    public function setSteps(array $steps): WorkflowConfiguration
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getSerializableGroups(): array
    {
        return $this->serializableGroups;
    }

    /**
     * @param string[] $serializableGroups
     * @return WorkflowConfiguration
     */
    public function setSerializableGroups(array $serializableGroups): WorkflowConfiguration
    {
        $this->serializableGroups = $serializableGroups;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartStep()
    {
        return $this->steps[0];
    }

    /**
     * @return string
     */
    public function getEndStep()
    {
        return $this->steps[count($this->steps) - 1];
    }
}
