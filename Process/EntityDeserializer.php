<?php

namespace Coosos\VersioningWorkflowBundle\Process;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Interface EntityDeserializer
 *
 * @package Coosos\VersioningWorkflowBundle\Process
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
interface EntityDeserializer
{
    /**
     * @param string                 $jsonObject
     * @param EntityDeserializer     $object
     * @param EntityManagerInterface $entityManager
     * @return EntityDeserializer
     */
    public function versioningWorkflowDeserialize($jsonObject, $object, EntityManagerInterface $entityManager);
}
