<?php

namespace Coosos\VersioningWorkflowBundle\Tests\Model;

/**
 * Class User
 *
 * @package Coosos\VersioningWorkflowBundle\Tests\Model
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class User
{
    private $id;
    private $username;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
}
