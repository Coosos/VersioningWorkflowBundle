<?php

namespace Coosos\VersioningWorkflowBundle\Tests\Serializer;

use Coosos\VersioningWorkflowBundle\Process\EntityDeserializer;
use Coosos\VersioningWorkflowBundle\Tests\Model\News;
use Coosos\VersioningWorkflowBundle\Tests\Model\User;
use Coosos\VersioningWorkflowBundle\Tests\VersioningWorkflowTestCase;

/**
 * Class SerializerTest
 *
 * @package Coosos\VersioningWorkflowBundle\Tests\Serializer
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class SerializerTest extends VersioningWorkflowTestCase
{
    public function testSerializeWithNotRelation()
    {
        $arr = ['id' => 1, 'title' => 'New bundle', 'content' => 'This bundle ....', 'user' => null];

        $object = (new News())->setId(1)->setTitle('New bundle')->setContent('This bundle ....');

        $this->assertEquals($this->serializer->serialize($object, ['test']), json_encode($arr));
    }

    public function testSerializeWithRelation()
    {
        $user = (new User())->setId(1)->setUsername('Remy');
        $actuality = (new News())->setId(1)->setTitle('New bundle')->setContent('This bundle ....')->setUser($user);

        $arr = ['id' => 1, 'title' => 'New bundle', 'content' => 'This bundle ....',
            'user' => ['id'=> 1, 'username' => 'Remy']];

        $this->assertEquals($this->serializer->serialize($actuality, ['test']), json_encode($arr));
    }

    public function testDeSerialize()
    {
        $mock = $this->generateMockEntityManager();

        $user = (new User())->setId(1)->setUsername('Remy');
        $actuality = (new News())->setId(1)->setTitle('New bundle')->setContent('This bundle ....')->setUser($user);

        $arr = ['id' => 1, 'title' => 'New bundle', 'content' => 'This bundle ....',
            'user' => ['id'=> 1, 'username' => 'Remy']];

        /** @var EntityDeserializer $deserialize */
        $deserialize = $this->serializer->deserialize(json_encode($arr), News::class);
        $deserialize = $deserialize->versioningWorkflowDeserialize($deserialize, $mock);
        $this->assertEquals($deserialize, $actuality);
    }
}
