<?php

namespace Coosos\VersioningWorkflowBundle\Process;

use Coosos\VersioningWorkflowBundle\Configuration\WorkflowConfiguration;
use Coosos\VersioningWorkflowBundle\Entity\VersioningWorkflow;
use Coosos\VersioningWorkflowBundle\Serializer\Adapter\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Process
 *
 * @package Coosos\VersioningWorkflowBundle\VersioningWorkflow
 * @author  Remy Lescallier <lescallier1@gmail.com>
 */
class Process extends AbstractProcess
{
    /**
     * @var SerializerInterface serializer
     */
    private $serializer;

    /**
     * @var EntityManagerInterface entityManager
     */
    private $entityManager;

    /**
     * @var string entityClass
     */
    private $entityClass;

    /**
     * Process constructor.
     *
     * @param SerializerInterface    $serializer
     * @param EntityManagerInterface $entityManager
     * @param WorkflowConfiguration  $configuration
     * @param string                 $entityClass
     */
    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        WorkflowConfiguration $configuration,
        string $entityClass
    ) {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->configuration = $configuration;
        $this->entityClass = $entityClass;
    }

    /**
     * Persist object to versioning entity by a step status
     *
     * @param EntityDeserializer | VersioningWorkflow $object
     * @param string                                  $step
     *
     * @return VersioningWorkflow
     */
    public function to($object, string $step)
    {
        if ($object instanceof EntityDeserializer) {
            $this->entityManager->detach($object);
        }

        $workflowName = $this->configuration->getWorkflowName();

        $versioningWorkflow = (new VersioningWorkflow())
            ->setWorkflowName($workflowName)
            ->setClassPath($this->entityClass)
            ->setActualStatus($step);

        if ($object instanceof VersioningWorkflow) {
            $compare = $this->compareStep($object->getActualStatus(), $step);
            $versioningWorkflow
                ->setOld($object)
                ->setInstance($object->getInstance())
                ->setJsonContent($this->serializer->serialize($object->getDeserializedContent()));

            if ($compare === 0) {
                $this->entityManager->remove($object);
                $versioningWorkflow->setOld($object->getOld());
                $versioningWorkflow->setId($object->getId());
            } else {
                if ($compare > 0) {
                    $instance = $this->entityManager
                        ->getRepository('CoososVersioningWorkflowBundle:VersioningWorkflow')
                        ->getInstanceAvailable($workflowName, $this->entityClass);

                    $versioningWorkflow
                        ->setInstance($instance);
                }
            }
        } else {
            $instance = $this->entityManager
                ->getRepository('CoososVersioningWorkflowBundle:VersioningWorkflow')
                ->getInstanceAvailable($workflowName, $this->entityClass);

            $versioningWorkflow
                ->setInstance($instance)
                ->setJsonContent($this->serializer->serialize($object));
        }

        $this->entityManager->persist($versioningWorkflow);

        return $versioningWorkflow;
    }

    /**
     * Persist object to versioning entity by start step status
     *
     * @param EntityDeserializer | VersioningWorkflow $object
     *
     * @return VersioningWorkflow
     */
    public function toStart($object)
    {
        return $this->to($object, $this->configuration->getStartStep());
    }

    /**
     * Persist object to versioning entity by end step status
     *
     * @param EntityDeserializer $object
     *
     * @return VersioningWorkflow
     */
    public function toEnd($object)
    {
        return $this->to($object, $this->configuration->getEndStep());
    }

    /**
     * {@inheritdoc}
     */
    public function getListBySteps(array $steps, $merged = false, $last = true)
    {
        $list = $this->entityManager
            ->getRepository('CoososVersioningWorkflowBundle:VersioningWorkflow')
            ->getList($steps, $this->configuration->getWorkflowName(), $this->entityClass, $merged, $last);

        foreach ($list as $item) {
            /** @var EntityDeserializer $deserialize */
            $deserialize = $this->serializer->deserialize(
                $item->getJsonContent(),
                $item->getClassPath()
            );

            $deserialize = $deserialize->versioningWorkflowDeserialize($item->getJsonContent(), $deserialize, $this->entityManager);
            $item->setDeserializedContent($deserialize);
        }

        return $list;
    }

    /**
     * Get object by id
     *
     * @param int $id
     *
     * @return VersioningWorkflow
     */
    public function getVersioningWorkflowEntityById(int $id)
    {
        $vwEntity = $this->entityManager->getRepository('CoososVersioningWorkflowBundle:VersioningWorkflow')->find($id);
        if ($vwEntity) {
            /** @var EntityDeserializer $deserialize */
            $deserialize = $this->serializer->deserialize(
                $vwEntity->getJsonContent(),
                $vwEntity->getClassPath()
            );

            $deserialize = $deserialize->versioningWorkflowDeserialize($vwEntity->getJsonContent(), $deserialize, $this->entityManager);
            $vwEntity->setDeserializedContent($deserialize);
        }

        return $vwEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function merge($object)
    {
        if ($object instanceof VersioningWorkflow) {
//            $find = $this->entityManager->getRepository($this->entityClass)->find($object->getDeserializedContent()->getId());
//            $this->entityManager->remove($find);
            return $this->entityManager->merge($object->setMerged(true)->getDeserializedContent());
        } elseif ($object instanceof EntityDeserializer) {
            return $this->entityManager->merge($object);
        }

        return null;
    }
}
