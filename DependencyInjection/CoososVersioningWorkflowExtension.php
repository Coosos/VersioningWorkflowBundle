<?php

namespace Coosos\VersioningWorkflowBundle\DependencyInjection;

use Coosos\VersioningWorkflowBundle\Serializer\Serializer;
use Coosos\VersioningWorkflowBundle\Service\VersioningWorkflowService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class CoososVersioningWorkflowExtension
 *
 * @package Coosos\VersioningWorkflowBundle\DependencyInjection
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class CoososVersioningWorkflowExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = $this->processConfiguration(new Configuration(), $configs);

        $serializerDefinition = new Definition(Serializer::class);
        $serializerDefinition->addArgument(new Reference('serializer'));
        $serializerDefinition->setPublic(false);
        $container->setDefinition('coosos.versioning_workflow.serializer.symfony', $serializerDefinition);

        foreach ($config["list"] as $key => $item) {
            $definition = new Definition(VersioningWorkflowService::class);
            $definition->addArgument(new Reference('coosos.versioning_workflow.serializer.symfony'));
            $definition->addArgument(new Reference('doctrine.orm.entity_manager'));
            $definition->addArgument($key);
            $definition->addArgument($item);
            $definition->setPublic(true);

            $container->setDefinition("coosos.versioning_workflow." . $key, $definition);
        }
    }
}
