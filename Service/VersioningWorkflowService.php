<?php

namespace Coosos\VersioningWorkflowBundle\Service;

use Coosos\VersioningWorkflowBundle\Configuration\WorkflowConfiguration;
use Coosos\VersioningWorkflowBundle\Process\AbstractProcess;
use Coosos\VersioningWorkflowBundle\Serializer\Adapter\SerializerInterface;
use Coosos\VersioningWorkflowBundle\Process\Process;
use Doctrine\ORM\EntityManager;

/**
 * Class VersioningWorkflowService
 *
 * @package Coosos\VersioningWorkflowBundle\Service
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class VersioningWorkflowService
{
    /**
     * @var WorkflowConfiguration workflowConfiguration
     */
    private $workflowConfiguration;

    /**
     * @var SerializerInterface serializer
     */
    private $serializer;

    /**
     * @var EntityManager entityManager
     */
    private $entityManager;

    /**
     * VersioningWorkflowService constructor.
     * @param SerializerInterface $serializer
     * @param EntityManager       $entityManager
     * @param string              $workflowName
     * @param array               $configurations
     */
    public function __construct(
        SerializerInterface $serializer,
        EntityManager $entityManager,
        string $workflowName,
        array $configurations
    ) {
        $this->workflowConfiguration = new WorkflowConfiguration($workflowName, $configurations);
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $entityClass
     * @return AbstractProcess
     */
    public function prepare(string $entityClass)
    {
        return new Process($this->serializer, $this->entityManager, $this->workflowConfiguration, $entityClass);
    }
}
