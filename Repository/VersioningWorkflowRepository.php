<?php

namespace Coosos\VersioningWorkflowBundle\Repository;

use Coosos\VersioningWorkflowBundle\Entity\VersioningWorkflow;
use Doctrine\ORM\EntityRepository;

/**
 * Class VersioningWorkflowRepository
 *
 * @package Coosos\VersioningWorkflowBundle\Repository
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class VersioningWorkflowRepository extends EntityRepository
{
    /**
     * @param array  $steps
     * @param string $workflowName
     * @param string $classPath
     * @param bool   $merged
     * @param bool   $last
     *
     * @return VersioningWorkflow[]
     */
    public function getList($steps, $workflowName = '', $classPath = '', $merged, $last)
    {
        $queryBuilder = $this->createQueryBuilder('vw');
        $queryBuilder
            ->andWhere($queryBuilder->expr()->in('vw.actualStatus', ':status'))
            ->setParameter(':status', $steps);

        if (!$last) {
            $tabs = [];
            $queryBuilderNotIn = $this->createQueryBuilder('vw')
                ->select('old.id')
                ->join('vw.old', 'old')
                ->andWhere('vw.old IS NOT NULL')
                ->getQuery()->getResult();

            foreach ($queryBuilderNotIn as $item) {
                $tabs[] = $item['id'];
            }

            if (!empty($tabs)) {
                $queryBuilder->andWhere($queryBuilder->expr()->notIn('vw.id', $tabs));
            }
        }

        if ($last) {
            $queryBuilderNotIn = $this->createQueryBuilder('vw2')
                ->select('IDENTITY(vw2.old)')
                ->andWhere('IDENTITY(vw2.old) = vw.id')
                ->getQuery();

            $queryBuilder->andWhere($queryBuilder->expr()->notIn('vw.id', $queryBuilderNotIn->getDQL()));
        }

        if (!empty($workflowName)) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('vw.workflowName', ':workflowName'))
                ->setParameter(':workflowName', $workflowName);
        }

        if (!empty($classPath)) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('vw.classPath', ':classPath'))
                ->setParameter(':classPath', $classPath);
        }

        if (!$merged) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('vw.merged', ':merged'))
                ->setParameter(':merged', $merged);
        }

//        dump($queryBuilder->getQuery()->getSQL());

        return $queryBuilder->getQuery()->getResult();
    }

    public function getInstanceAvailable($workflowName, $classPath)
    {
        $queryBuilder = $this->createQueryBuilder('vw');
        $queryBuilder = $queryBuilder->select('vw.instance')
            ->andWhere($queryBuilder->expr()->eq('vw.workflowName', ':workflowName'))
            ->setParameter(':workflowName', $workflowName)
            ->andWhere($queryBuilder->expr()->eq('vw.classPath', ':classPath'))
            ->setParameter(':classPath', $classPath)
            ->orderBy($queryBuilder->expr()->desc('vw.instance'))
            ->setMaxResults(1)
            ->getQuery();

        $result = $queryBuilder->getScalarResult();

        $available = 1;
        if (!empty($result)) {
            $available += $result[0]['instance'];
        }

        return $available;
    }
}
