<?php

namespace Coosos\VersioningWorkflowBundle\Tests\Process;

use Coosos\VersioningWorkflowBundle\Process\Process;
use Coosos\VersioningWorkflowBundle\Repository\VersioningWorkflowRepository;
use Coosos\VersioningWorkflowBundle\Tests\Model\User;
use Coosos\VersioningWorkflowBundle\Tests\VersioningWorkflowTestCase;

/**
 * Class ProcessTest
 *
 * @package Coosos\VersioningWorkflowBundle\Tests\Process
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class ListTest extends VersioningWorkflowTestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testGetListWithoutRelation()
    {
        $news = $this->generateNews();
        $versioningWorkflow = $this->generateVersioningWorkflow($news);
        $versioningWorkflowResult = $this->generateVersioningWorkflow($news)->setDeserializedContent($news);

        $process = $this->generateProcess($versioningWorkflow);

        $this->assertEquals($process->getListBySteps(['editor']), [$versioningWorkflowResult]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetListWithRelation()
    {
        $user = (new User())->setId(1)->setUsername('Remy');
        $news = $this->generateNews(null, $user);
        $versioningWorkflow = $this->generateVersioningWorkflow($news);
        $versioningWorkflowResult = $this->generateVersioningWorkflow($news)->setDeserializedContent($news);

        $process = $this->generateProcess($versioningWorkflow);

        $this->assertEquals($process->getListBySteps(['editor']), [$versioningWorkflowResult]);
    }

    /**
     * @param $versioningWorkflow
     *
     * @return Process
     * @throws \ReflectionException
     */
    private function generateProcess($versioningWorkflow)
    {
        $mockRepository = $this->createMock(VersioningWorkflowRepository::class);
        $mockRepository->method('getList')->willReturn([$versioningWorkflow]);

        $mockEntityManager = $this->generateMockEntityManager();
        $mockEntityManager->method('getRepository')->willReturn($mockRepository);

        return $this->getProcess($mockEntityManager);
    }
}
