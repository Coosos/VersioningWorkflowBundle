<?php

namespace Coosos\VersioningWorkflowBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CoososVersioningWorkflowBundle
 *
 * @package Coosos\VersioningWorkflowBundle
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class CoososVersioningWorkflowBundle extends Bundle
{
}
