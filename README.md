*Deprecated : See VWorkflowBundle*

## Usage

### Configuration

#### Example

    coosos_versioning_workflow:
        list:
            news: # Workflow name
                serializable_groups: # Group name for serialization
                    - example_group1
                    - example_group2
                steps: # List of different status | Caution in the order of the status list
                    - writing
                    - validation
                    - publish
