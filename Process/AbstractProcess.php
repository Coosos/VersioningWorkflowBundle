<?php

namespace Coosos\VersioningWorkflowBundle\Process;

use Coosos\VersioningWorkflowBundle\Configuration\WorkflowConfiguration;
use Coosos\VersioningWorkflowBundle\Entity\VersioningWorkflow;

/**
 * Class AbstractProcess
 *
 * @package Coosos\VersioningWorkflowBundle\Process
 * @author  Remy Lescallier <lescallier1@gmail.com>
 */
abstract class AbstractProcess
{
    /**
     * @var WorkflowConfiguration configuration
     */
    protected $configuration;

    /**
     * Persist object to versioning entity by a step status
     *
     * @param EntityDeserializer $object
     * @param string             $step
     * @return VersioningWorkflow
     */
    abstract public function to($object, string $step);

    /**
     * Persist object to versioning entity by start step status
     *
     * @param EntityDeserializer $object
     * @return VersioningWorkflow
     */
    abstract public function toStart($object);

    /**
     * Persist object to versioning entity by end step status
     *
     * @param EntityDeserializer $object
     * @return VersioningWorkflow
     */
    abstract public function toEnd($object);

    /**
     * Get list object by steps
     *
     * @param array $steps
     * @param bool  $merged
     * @param bool  $last
     *
     * @return VersioningWorkflow[]
     */
    abstract public function getListBySteps(array $steps, $merged = false, $last = true);

    /**
     * Get object by id
     *
     * @param int $id
     * @return VersioningWorkflow
     */
    abstract public function getVersioningWorkflowEntityById(int $id);

    /**
     * Merge data on original table
     *
     * @param mixed $object Object merge
     *
     * @return mixed
     */
    abstract public function merge($object);

    /**
     * @return WorkflowConfiguration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Comparing the steps with respect to the configuration to determine whether the original step is located before
     * or after the step called
     *
     * -1 = original is first draw
     * 1 = next is first draw
     * 0 = original and next is equals
     *
     * @param string $originalStep
     * @param string $nextStep
     *
     * @return int
     */
    protected function compareStep(string $originalStep, string $nextStep)
    {
        $tmpSteps = [];
        foreach ($this->configuration->getSteps() as $step) {
            $tmpSteps[] = $step;
            if (in_array($originalStep, $tmpSteps) && !in_array($nextStep, $tmpSteps)) {
                return -1;
            } elseif (!in_array($originalStep, $tmpSteps) && in_array($nextStep, $tmpSteps)) {
                return 1;
            }
        }

        return 0;
    }
}
