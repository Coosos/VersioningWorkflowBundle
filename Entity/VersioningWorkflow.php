<?php

namespace Coosos\VersioningWorkflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VersioningWorkflow
 *
 * @package Coosos\VersioningWorkflowBundle\Entity
 * @author Remy Lescallier <lescallier1@gmail.com>
 *
 * @ORM\Entity(repositoryClass="Coosos\VersioningWorkflowBundle\Repository\VersioningWorkflowRepository")
 */
class VersioningWorkflow
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string workflowName
     * @ORM\Column(type="string", length=255)
     */
    private $workflowName;

    /**
     * @var string classPath
     * @ORM\Column(type="string", length=255)
     */
    private $classPath;

    /**
     * Is null if the object was not already publish
     *
     * @var string|null entityOriginalJsonKey
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entityOriginalJsonKey;

    /**
     * @var integer instance
     * @ORM\Column(type="integer")
     */
    private $instance;

    /**
     * @var string actualStatus
     * @ORM\Column(type="string", length=255)
     */
    private $actualStatus;

    /**
     * @var string jsonContent
     * @ORM\Column(type="text")
     */
    private $jsonContent;

    /**
     * @var VersioningWorkflow|null old
     * @ORM\ManyToOne(targetEntity="Coosos\VersioningWorkflowBundle\Entity\VersioningWorkflow")
     * @ORM\JoinColumn(nullable=true)
     */
    private $old;

    /**
     * @var bool merged
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $merged = false;

    /**
     * @var mixed deserializedContent
     */
    private $deserializedContent;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkflowName(): string
    {
        return $this->workflowName;
    }

    /**
     * @param string $workflowName
     * @return VersioningWorkflow
     */
    public function setWorkflowName(string $workflowName): VersioningWorkflow
    {
        $this->workflowName = $workflowName;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassPath(): string
    {
        return $this->classPath;
    }

    /**
     * @param string $classPath
     * @return VersioningWorkflow
     */
    public function setClassPath(string $classPath): VersioningWorkflow
    {
        $this->classPath = $classPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityOriginalJsonKey()
    {
        return $this->entityOriginalJsonKey;
    }

    /**
     * @param string $entityOriginalJsonKey
     * @return VersioningWorkflow
     */
    public function setEntityOriginalJsonKey($entityOriginalJsonKey): VersioningWorkflow
    {
        $this->entityOriginalJsonKey = $entityOriginalJsonKey;
        return $this;
    }

    /**
     * @return int
     */
    public function getInstance(): int
    {
        return $this->instance;
    }

    /**
     * @param int $instance
     * @return VersioningWorkflow
     */
    public function setInstance(int $instance): VersioningWorkflow
    {
        $this->instance = $instance;
        return $this;
    }

    /**
     * @return string
     */
    public function getActualStatus(): string
    {
        return $this->actualStatus;
    }

    /**
     * @param string $actualStatus
     * @return VersioningWorkflow
     */
    public function setActualStatus(string $actualStatus): VersioningWorkflow
    {
        $this->actualStatus = $actualStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getJsonContent(): string
    {
        return $this->jsonContent;
    }

    /**
     * @param string $jsonContent
     * @return VersioningWorkflow
     */
    public function setJsonContent(string $jsonContent): VersioningWorkflow
    {
        $this->jsonContent = $jsonContent;
        return $this;
    }

    /**
     * @return VersioningWorkflow|null
     */
    public function getOld()
    {
        return $this->old;
    }

    /**
     * @param VersioningWorkflow $old
     * @return VersioningWorkflow
     */
    public function setOld($old)
    {
        $this->old = $old;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeserializedContent()
    {
        return $this->deserializedContent;
    }

    /**
     * @param mixed $deserializedContent
     * @return VersioningWorkflow
     */
    public function setDeserializedContent($deserializedContent)
    {
        $this->deserializedContent = $deserializedContent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMerged(): bool
    {
        return $this->merged;
    }

    /**
     * @param bool $merged
     *
     * @return VersioningWorkflow
     */
    public function setMerged(bool $merged): VersioningWorkflow
    {
        $this->merged = $merged;

        return $this;
    }
}
