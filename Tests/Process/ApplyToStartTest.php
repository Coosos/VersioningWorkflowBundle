<?php

namespace Coosos\VersioningWorkflowBundle\Tests\Process;

use Coosos\VersioningWorkflowBundle\Repository\VersioningWorkflowRepository;
use Coosos\VersioningWorkflowBundle\Tests\VersioningWorkflowTestCase;

/**
 * Class ApplyToStartTest
 *
 * @package Coosos\VersioningWorkflowBundle\Tests\Process
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
class ApplyToStartTest extends VersioningWorkflowTestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testApplyToStartWithNewsEntity()
    {
        $news = $this->generateNews();
        $versioningWorkflow = $this->generateVersioningWorkflow($news);

        $mockRepository = $this->createMock(VersioningWorkflowRepository::class);
        $mockRepository->method('getInstanceAvailable')->willReturn(1);

        $mockEntityManager = $this->generateMockEntityManager();
        $mockEntityManager->method('getRepository')->willReturn($mockRepository);
        $mockEntityManager->method('persist')->willReturn($this->returnValue(null));

        $process = $this->getProcess($mockEntityManager);

        $this->assertEquals($process->toStart($news), $versioningWorkflow);
    }

    /**
     * @throws \ReflectionException
     */
    public function testApplyToStartWithVersioningWorkflowEntity()
    {
        $news = $this->generateNews();

        $versioningWorkflow = $this->generateVersioningWorkflow($news, 1, 'publish');
        $versioningWorkflow
            ->setDeserializedContent(
                $news->versioningWorkflowDeserialize(
                    $this->serializer->deserialize($versioningWorkflow->getJsonContent(), self::ENTITY_PATH),
                    $this->generateMockEntityManager()
                )
            );

        $versioningWorkflow2 = $this->generateVersioningWorkflow($news, 2);
        $versioningWorkflow2->setOld($versioningWorkflow);

        $mockRepository = $this->createMock(VersioningWorkflowRepository::class);
        $mockRepository->method('getInstanceAvailable')->willReturn(2);

        $mockEntityManager = $this->generateMockEntityManager();
        $mockEntityManager->method('getRepository')->willReturn($mockRepository);
        $mockEntityManager->method('persist')->willReturn($this->returnValue(null));

        $process = $this->getProcess($mockEntityManager);
        $this->assertEquals($process->toStart($versioningWorkflow), $versioningWorkflow2);
    }
}
