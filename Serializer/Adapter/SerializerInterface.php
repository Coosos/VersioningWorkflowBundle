<?php

namespace Coosos\VersioningWorkflowBundle\Serializer\Adapter;

/**
 * Interface SerializerInterface
 *
 * @package Coosos\VersioningWorkflowBundle\Serializer\Adapter
 * @author Remy Lescallier <lescallier1@gmail.com>
 */
interface SerializerInterface
{
    public function serialize($object, $groups = []);
    public function deserialize($object, $class);
}
